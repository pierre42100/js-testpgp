/**
 * PGP Helper
 * 
 * @author Pierre HUBERT
 */

class PublicKeyPair {
    constructor(pubkey) {
        this.pubkey = pubkey;
    }
}

class PublicPrivateKeyPair extends PublicKeyPair {
    constructor(pubkey, privkey) {
        super(pubkey)
        this.privkey = privkey;
    }
}

class PGPHelper {
    
    static async init() {
        await openpgp.initWorker({path: "openpgp/openpgp.worker.min.js"})
    }

    static async generateKeyPair(name, email, numbits, password) {

        const options = {
            userIds: [{ name: name, email: email }], // multiple user IDs
            numBits: numbits,   // RSA key size
            passphrase: password // protects the private key
        };

        const result = await openpgp.generateKey(options);
    
        return new PublicPrivateKeyPair(
            result.publicKeyArmored,
            result.privateKeyArmored
        );
        
    }

    /**
     * Get the fingerprint of a key
     * 
     * @param {PublicKeyPair} keyPair Target key
     */
    static async getFingerPrint(keyPair) {
        const pubkey = (await openpgp.key.readArmored(keyPair.pubkey)).keys[0];
        return pubkey.keyPacket.fingerprint;
    }

    static async sign(keyPair, password, message) {

        const  privKeyObj = (await openpgp.key.readArmored(keyPair.privkey)).keys[0];
        await privKeyObj.decrypt(password);

        const options = {
            message: openpgp.cleartext.fromText(message), // CleartextMessage or Message object
            privateKeys: [privKeyObj],                        // for signing
            
        };

        return (await openpgp.sign(options)).data;
    }

    static async verify(keyPair, signature) {
        const options = {
            message: await openpgp.cleartext.readArmored(signature), // parse armored message
            publicKeys: (await openpgp.key.readArmored(keyPair.pubkey)).keys // for verification
        };
        
        const result = await openpgp.verify(options)

        if(!result.signatures[0].valid)
            throw Error("Could not verify message signature !");

        return result.data;
        
    }

    /**
     * Message to encrypt
     * 
     * @param {PublicKeyPair} keyPair 
     * @param {String} message 
     */
    static async cryptString(keyPair, message) {
        
        const options = {
            message: openpgp.message.fromText(message),       // input as Message object
            publicKeys: (await openpgp.key.readArmored(keyPair.pubkey)).keys, // for encryption
        }

        const result = await openpgp.encrypt(options);

        return result.data;
    }

    /**
     * Decrypt string
     * 
     * @param {PublicPrivateKeyPair} keyPair
     * @param {String} password
     * @param {String} encrypted
     */
    static async decryptString(keyPair, password, encrypted) {

        const privKeyObj = (await openpgp.key.readArmored(keyPair.privkey)).keys[0]
        await privKeyObj.decrypt(password)

        const options = {
            message: await openpgp.message.readArmored(encrypted),    // parse armored message
            privateKeys: [privKeyObj]                                 // for decryption
        }

        const result = await openpgp.decrypt(options);

        return result.data;
    }
}